#MD2File

## 为什么会有MD2File这个开源工具？

万事皆有因果，所以这东西也不是闲的蛋疼才搞出来的。当初是有个需求，把文本内容导出到word。粗略一想，好像不难。估计网上大把大把的资料。

找了一圈，发现成型的做法基本没有。有个html转word的方式，使用poi组件的。使用了下，可以满足需求，缺点也比较明显：

1. 兼容性不强，mac中用pages是打不开的。
2. 图片非本地保存，是联网图片。这意味着，用户断网之后，打开这个文档，是看不到图片的。

这两点是比较致命的缺陷，看起来就不够专业。

想了想，既然没有这方面的成品可以使用，那就自己写一个吧。于是，就写了这么个东西。

## MD2File可以干嘛？

顾名思义，这是一个markdown文本转word，pdf等文档的工具类，后期会继续扩展支持更多类型的文档。

之所以使用markdown，是因为markdown比较好解析，而且md文本的内容会比较规范。另外，html转md也是比较好处理的。

目前MD2File支持大部分markdown的基本语法（**支持表格语法**），暂不支持的语法有：链接（link），无序和有序列表。

* 链接
* 无序和有序列表：这个其实使用原生的markdown文本输出貌似就很不错了。

MD2File导出的word文档，在微软的office word中格式是最好的，毕竟poi开发的时候，也是以支持ms word为主。在wps中也还不错。在pages中内容排版基本正常，部分样式不支持。导出的pdf文档，相对于word文档，会美观很多。

## 怎么获取MD2File这个开源工具？

代码已经放到：[https://git.oschina.net/cevin15/MD2File](https://git.oschina.net/cevin15/MD2File)

有兴趣的可以star一下，想使用的可以fork一下。

## 关于MD2File的一点说明

使用很简单，用`FileFactory`提供的方法即可。导出word依赖于poi，pdf依赖于itext，通过pom.xml文件可以清楚看到。

如果觉得默认的样式不符合自己的要求，可以fork项目之后，通过修改`*Decorator`这个类来实现。

为方便大家下载直接使用，在lib中上传了MD2File的jar包，以及依赖包。

最后在发一次地址：[https://git.oschina.net/cevin15/MD2File](https://git.oschina.net/cevin15/MD2File)