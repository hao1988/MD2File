package net.oschina.md2.markdown.builder;

import net.oschina.md2.markdown.Block;
import net.oschina.md2.markdown.BlockType;
import net.oschina.md2.markdown.MDToken;
import net.oschina.md2.markdown.ValuePart;

public class HeaderBuilder implements BlockBuilder{

	private String content;
	public HeaderBuilder(String content){
		this.content = content;
	}
	
	public Block bulid() {
		Block block = new Block();
		int i = content.lastIndexOf(MDToken.HEADLINE);
		
		block.setType(BlockType.HEADLINE);
		block.setValueParts(new ValuePart(content.substring(i+1).trim()));
		block.setLevel(i);
		return block;
	}
	
	public Block bulid(int level) {
		Block block = new Block();
		
		block.setType(BlockType.HEADLINE);
		block.setValueParts(new ValuePart(content));
		block.setLevel(level);
		return block;
	}

	public boolean isRightType() {
		return content.startsWith(MDToken.HEADLINE);
	}

	public static int isRightType(String nextLineStr){
		if(!nextLineStr.startsWith("-") && !nextLineStr.startsWith("=")){
			return 0;
		}
		String tmpS = nextLineStr.replaceAll("-", "").trim();
		if(tmpS.length()==0){
			return 2;
		}
		tmpS = nextLineStr.replaceAll("=", "").trim();
		if(tmpS.length()==0){
			return 1;
		}
		return 0;
	}
	
}
