package net.oschina.md2.markdown.builder;

import java.util.List;

import net.oschina.md2.markdown.Block;
import net.oschina.md2.markdown.BlockType;
import net.oschina.md2.markdown.MDAnalyzer;
import net.oschina.md2.markdown.MDToken;
import net.oschina.md2.markdown.ValuePart;

public class QuoteBuilder implements BlockBuilder{

	private String content;
	public QuoteBuilder(String content){
		this.content = content;
	}
	
	public Block bulid() {
		Block block = new Block();
		String value = content.substring(1).trim();

		if(value.trim().equals("")){	//空行直接忽略
			return null;
		}
		int i = 0;
		if(value.trim().startsWith(MDToken.HEADLINE)){	//检查是否有标题格式
			i = value.lastIndexOf(MDToken.HEADLINE);
		}
		if(i>0){
			value = value.substring(i+1).trim();
		}
		
		List<ValuePart> list = MDAnalyzer.analyzeTextLine(value);
		if(i>0){
			for (ValuePart valuePart : list) {
				valuePart.addType(BlockType.HEADLINE);
				valuePart.setLevel(i);
			}
		}
		
		block.setType(BlockType.QUOTE);
		block.setValueParts(list);
		return block;
	}

	public boolean isRightType() {
		return false;
	}

}
