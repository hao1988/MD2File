package net.oschina.test;

import java.io.File;
import java.io.FileNotFoundException;

import net.oschina.md2.export.FileFactory;

import org.junit.Test;

public class ProduceTest {

	@Test
	public void test(){
		try {
			FileFactory.produce(new File("test_file/md_for_test.md"), "test_file/test.docx");
			FileFactory.produce(new File("test_file/md_for_test.md"), "test_file/test.pdf");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}